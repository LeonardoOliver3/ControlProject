package br.com.lp3.controlproject.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.AtividadeDTO;
import br.com.lp3.controlproject.model.Atividade;
import br.com.lp3.controlproject.repository.AtividadeRepository;

@Service
public class AtividadeService {

	@Autowired
	private AtividadeRepository atividadeRepository;
	
	public AtividadeDTO findById(Long id) {
		Optional<Atividade> oAtividade = atividadeRepository.findById(id);
		if (oAtividade != null && oAtividade.isPresent()) {
			Atividade atividade = oAtividade.get();
			AtividadeDTO atividadeDTO = new AtividadeDTO(atividade.getId(), atividade.getDescricao(), atividade.getStatus(), atividade.getUsuario(), atividade.getTarefa());
			return atividadeDTO;
		}else {
			return null;
		}
	}
	
	public AtividadeDTO saveAtividade(AtividadeDTO atividadeDTO) {
		Atividade atividade = new Atividade(atividadeDTO.getId(), atividadeDTO.getDescricao(), atividadeDTO.getStatus(), atividadeDTO.getUsuario(), atividadeDTO.getTarefa());
		atividade = atividadeRepository.save(atividade);
		atividadeDTO.setId(atividade.getId());
		return atividadeDTO;
	}
	
	
}