package br.com.lp3.controlproject.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.el.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.controlproject.dto.ProjetoDTO;
import br.com.lp3.controlproject.dto.StatusDTO;
import br.com.lp3.controlproject.model.Projeto;
import br.com.lp3.controlproject.model.Status;
import br.com.lp3.controlproject.repository.ProjetoRepository;



@Service
public class ProjetoService {

	@Autowired
	private ProjetoRepository projetoRepository;

	public ProjetoDTO findById(Long id) {
		Optional<Projeto> oProjeto = projetoRepository.findById(id);
		if (oProjeto != null && oProjeto.isPresent()) {
			Projeto projeto = oProjeto.get();
			ProjetoDTO projetoDTO = new ProjetoDTO();
			projetoDTO.setId(projeto.getId());
			projetoDTO.setNome(projeto.getNome());
			projetoDTO.setDescricao(projeto.getDescricao());
			Status status = projeto.getStatus();
			StatusDTO statusDTO = new StatusDTO(status.getId(), status.getStatus());
			projetoDTO.setStatus(statusDTO);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String dataInicio = df.format(projeto.getDataInicio());
			String dataLimite = df.format(projeto.getDataLimite());
			projetoDTO.setDataInicio(dataInicio);
			projetoDTO.setDataLimite(dataLimite);
			return projetoDTO;
		} else {
			return null;
		}

	}
	
	public ProjetoDTO saveProjeto(ProjetoDTO projetoDTO) {
		Projeto projeto = new Projeto();
		projeto.setId(projetoDTO.getId());
		projeto.setNome(projetoDTO.getNome());
		projeto.setDescricao(projetoDTO.getDescricao());
		StatusDTO statusDTO = projetoDTO.getStatus();
		Status status = new Status(statusDTO.getId(), statusDTO.getStatus());
		projeto.setStatus(status);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date dataInicio = df.parse(projetoDTO.getDataInicio());
			Date dataLimite = df.parse(projetoDTO.getDataLimite());
			projeto.setDataInicio(dataInicio);
			projeto.setDataLimite(dataLimite);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		projeto = projetoRepository.save(projeto);
		projetoDTO.setId(projeto.getId());
		return projetoDTO;
	}
	
	public List<ProjetoDTO> findAll() {
		Iterable<Projeto> iterableProjeto = projetoRepository.findAll();
		Iterator<Projeto> iteratorProjeto = iterableProjeto.iterator();
		List<ProjetoDTO> projetoDTOList = new ArrayList<>();
		while (iteratorProjeto.hasNext()) {
			Projeto projeto = iteratorProjeto.next();
			ProjetoDTO projetoDTO = new ProjetoDTO();
			projetoDTO.setId(projeto.getId());
			projetoDTO.setNome(projeto.getNome());
			projetoDTO.setDescricao(projeto.getDescricao());
			Status status = projeto.getStatus();
			StatusDTO statusDTO = new StatusDTO(status.getId(), status.getStatus());
			projetoDTO.setStatus(statusDTO);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String dataInicio = df.format(projeto.getDataInicio());
			String dataLimite = df.format(projeto.getDataLimite());
			projetoDTO.setDataInicio(dataInicio);
			projetoDTO.setDataLimite(dataLimite);
			projetoDTOList.add(projetoDTO);
		}
		return projetoDTOList;
	}
	
	public void removeProjeto(Long id) {
		projetoRepository.deleteById(id);
	}

}
