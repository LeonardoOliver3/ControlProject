package br.com.lp3.controlproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lp3.controlproject.dto.AtividadeDTO;
import br.com.lp3.controlproject.service.AtividadeService;

@RestController
@RequestMapping(value = "/atividade")
public class AtividadeController {
	
	@Autowired
	private AtividadeService atividadeService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<AtividadeDTO> findAtividadeById(@PathVariable Long id) {
		AtividadeDTO atividadeDTO = atividadeService.findById(id);
		if (atividadeDTO != null) {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<AtividadeDTO> saveAtividade(@RequestBody AtividadeDTO atividadeDTO) {
		atividadeDTO = atividadeService.saveAtividade(atividadeDTO);
		if (atividadeDTO != null) {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.PUT)
	public ResponseEntity<AtividadeDTO> editAtividade(@RequestBody AtividadeDTO atividadeDTO) {
		atividadeDTO = atividadeService.saveAtividade(atividadeDTO);
		if (atividadeDTO != null) {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.DELETE)
	public ResponseEntity<AtividadeDTO> deleteAtividade(@RequestBody AtividadeDTO atividadeDTO) {
		atividadeDTO = atividadeService.saveAtividade(atividadeDTO);
		if (atividadeDTO != null) {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<AtividadeDTO>(atividadeDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
}